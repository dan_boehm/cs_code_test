<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class RehashPasswords extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:rehash-passwords';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rehash the plain text passwords for all users.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach(User::cursor() as $user) {
            $user->password_hash = Hash::make($user->password_plain);
            $user->save();
        }

        return Command::SUCCESS;
    }
}
