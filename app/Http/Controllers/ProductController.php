<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProductRequest;
use App\Models\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        return Inertia::render("Product/Index", [
            "page" => Auth::user()->products()->paginate(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Inertia\Response
     */
    public function create()
    {
        return Inertia::render('Product/Create', [
            "fields" => StoreProductRequest::fields(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreProductRequest $request
     *
     * @return RedirectResponse
     */
    public function store(StoreProductRequest $request)
    {
        Auth::user()->products()->create($request->validated());

        return Redirect::route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Product $product
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Product $product
     *
     * @return \Inertia\Response
     */
    public function edit(Product $product)
    {
        return Inertia::render('Product/Edit', [
            'product' => $product->toArray(),
            'fields' => StoreProductRequest::fields(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreProductRequest $request
     * @param Product             $product
     *
     * @return RedirectResponse
     */
    public function update(StoreProductRequest $request, Product $product)
    {
        $product->update($request->validated());

        return Redirect::route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product  $product
     *
     * @return RedirectResponse
     */
    public function destroy(Product $product)
    {
        $this->authorize('delete', $product);

        $product->delete();

        return Redirect::route('products.index');
    }
}
