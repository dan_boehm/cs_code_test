<?php

namespace App\Http\Controllers;

use App\Models\InventoryItem;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = Auth::user()
                     ->inventory()
                     ->with(['product']);

        if ($value = $request->query('product_id')) {
            $items->where('product_id', $value);
        }
        if ($value = $request->query('sku')) {
            $items->where('sku', $value);
        }

        return Inertia::render("Inventory/Index", [
            "page" => $items->paginate(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InventoryItem $inventory
     *
     * @return \Illuminate\Http\Response
     */
    public function show(InventoryItem $inventory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\InventoryItem $inventory
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(InventoryItem $inventory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param \App\Models\InventoryItem $inventory
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InventoryItem $inventory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\InventoryItem $inventory
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(InventoryItem $inventory)
    {
        //
    }
}
