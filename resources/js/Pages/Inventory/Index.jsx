import React from "react";
// import { Inertia } from "@inertiajs/inertia";
import { router } from "@inertiajs/react";
import {Head, Link, usePage} from "@inertiajs/react";
import Table from "@/Components/Table";
import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import ModelList from "@/Components/ModelList";

export default class Index extends React.Component {
    render() {
        const columns = [
            ["ID", "id"],
            ["Product ID", "product_id"],
            ["Product Name", "product.product_name"],
            ["SKU", "sku"],
            ["Qty.", "quantity"],
            ["Color", "color"],
            ["Size", "size"],
            ["Price", "price"], // TODO: Need proper serialization of price/cost
            ["Cost", "cost"],
        ];

        const metrics = {
            "Total Items": this.props.page.total,
        }

        const filters = [
            {id: "product_id", name: "Product ID", value: null},
            {id: "sku", name: "SKU", value: null},
        ];

        const editRouter = (item) => {
            return route(`products.edit`, item.id);
        }

        console.log(this.props.page);

        return (
            <AuthenticatedLayout
                auth={this.props.auth}
                errors={this.props.errors}
                header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">Inventory</h2>}
            >
                <Head title="Inventory"/>

                <ModelList
                    columns={columns}
                    metrics={metrics}
                    filters={filters}
                    page={this.props.page}
                    editRouter={editRouter}
                    onSubmitFilters={(filters) => this.updatePage({filters})}
                />
            </AuthenticatedLayout>
        );
    }
};
