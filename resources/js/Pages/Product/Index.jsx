import React from "react";
import { Head } from "@inertiajs/react";
import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import ModelList from "@/Components/ModelList";

export default function Index(props) {
    const columns = [
        ["ID", "id"],
        // ["Admin ID", "admin_id"],
        ["Name", "product_name"],
        // ["Style", "style"],
        ["Brand", "brand"],
        ["Skus", null], // TODO: Need to grab/inject sku values efficiently.
    ]

    const metrics = {};

    const editRouter = (product) => {
        return route(`products.edit`, product.id);
    }
    const deleteRouter = (product) => {
        return route(`products.destroy`, product.id);
    }

    console.log(props.page);

    return (
        <AuthenticatedLayout
            auth={props.auth}
            errors={props.errors}
            header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">Products</h2>}
        >
            <Head title="Products" />

            <ModelList
                columns={columns}
                metrics={metrics}
                page={props.page}
                editRouter={editRouter}
                deleteRouter={deleteRouter}
                createable
            />
        </AuthenticatedLayout>
    );
};
