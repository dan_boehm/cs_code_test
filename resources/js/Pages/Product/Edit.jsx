import React from "react";
import {Head, Link, useForm} from "@inertiajs/react";
import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import ModelEditor from "@/Components/ModelEditor";

export default function Edit(props) {
    return (
        <AuthenticatedLayout
            auth={props.auth}
            errors={props.errors}
            header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">Products</h2>}
        >
            <Head title="Products" />

            <ModelEditor
                fields={props.fields}
                model={props.product}
                submitRoute={route('products.update', props.product.id)}
            />

        </AuthenticatedLayout>
    );
};
