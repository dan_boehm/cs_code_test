import React from "react";
import Table from "@/Components/Table";
import MetricList from "@/Components/MetricList";
import FilterList from "@/Components/FilterList";
import {Link, router} from "@inertiajs/react";
import PrimaryLink from "@/Components/PrimaryLink";
import SecondaryLink from "@/Components/SecondaryLink";

class ModelList extends React.Component {
    constructor(props) {
        super(props);

        const url = this.getURL();
        this.state = {
            filters: props.filters.map(filter => {
                filter.value = url.searchParams.get(filter.id);

                return filter;
            }),
        }
    }

    getURL = () => {
        return new URL(window.location.href);
    }

    updateList = ({filters}) => {
        const href = this.filterURL(this.getURL().href, filters)

        router.visit(href);
    }

    getFilterValues = () => {
        return this.state.filters.reduce((filterVals, filter) => {
            filterVals[filter.id] = filter.value;
            return filterVals;
        }, {});
    }

    filterURL = (url, filters = null) => {
        url = url ? new URL(url) : this.getURL();
        filters = filters || this.getFilterValues();

        Object.entries(filters).forEach(([name, value]) => {
            if (value) {
                url.searchParams.set(name, value);
            } else {
                url.searchParams.delete(name);
            }
        });

        return url.href;
    }

    submitFilters = (filters) => this.updateList({filters});

    render() {
        console.log(this.props.page);

        return (
            <div className="py-12">
                <div className="container mx-auto">
                    <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                        {this.props.createable &&
                            <div className="flex items-center justify-between mb-6">
                                <PrimaryLink
                                    href={route("products.create")}
                                >
                                    Create Product
                                </PrimaryLink>
                            </div>
                        }
                        <FilterList filters={this.state.filters} onSubmit={this.submitFilters}/>

                        <MetricList metrics={this.props.metrics}/>

                        <Table
                            columns={this.props.columns}
                            models={this.props.page.data}
                            editRouter={this.props.editRouter}
                            deleteRouter={this.props.deleteRouter}
                        />
                    </div>
                    <div className="flex items-center mb-6">
                        <SecondaryLink
                            // className="px-6 py-2 text-white bg-blue-500 rounded-md focus:outline-none"
                            href={this.filterURL(this.props.page.first_page_url)}
                        >
                            first
                        </SecondaryLink>
                        <PrimaryLink
                            // className="px-6 py-2 text-white bg-blue-500 rounded-md focus:outline-none"
                            href={this.filterURL(this.props.page.prev_page_url)}
                        >
                            prev
                        </PrimaryLink>
                        <span>
                            Page {this.props.page.current_page} of {this.props.page.last_page}
                        </span>
                        <PrimaryLink
                            // className="px-6 py-2 text-white bg-blue-500 rounded-md focus:outline-none"
                            href={this.filterURL(this.props.page.next_page_url)}
                        >
                            next
                        </PrimaryLink>
                        <SecondaryLink
                            // className="px-6 py-2 text-white bg-blue-500 rounded-md focus:outline-none"
                            href={this.filterURL(this.props.page.last_page_url)}
                        >
                            last
                        </SecondaryLink>
                    </div>
                </div>
            </div>
        )
    }
}

ModelList.defaultProps = {
    filters: [],
    metrics: {},
    editable: false,
}

export default ModelList
