import React from "react";

export default function MetricList(props) {
    return (
        <div>
            {Object.entries(props.metrics).map(([name, value]) => (
                <div>
                    <strong>{name}: </strong> {value}
                </div>
            ))}
        </div>
    )
}
