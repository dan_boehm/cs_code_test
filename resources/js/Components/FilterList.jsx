import React from "react";
import Filter from "@/Components/Filter";
import PrimaryButton from "@/Components/PrimaryButton";
import SecondaryButton from "@/Components/SecondaryButton";

class FilterList extends React.Component {
    constructor(props) {
        super(props);

        this.state = props.filters.reduce((filterVals, filter) => {
            filterVals[filter.id] = filter.value;

            return filterVals;
        }, {});
    }

    handleFilterValueChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value || null,
        });
    }

    clearFilters = () => {
        const cleared = {}
        for (const filter_id in this.state) {
            cleared[filter_id] = null;
        }
        this.props.onSubmit(cleared);
    }

    submitFilters = () => {
        console.log(this.state);
        this.props.onSubmit(this.state)
    }

    render() {
        const filters = this.props.filters || [];

        if (filters.length === 0) {
            return null;
        }

        return (
            <div>
                {this.props.filters.map(filter => (
                    <Filter
                        id={filter.id}
                        name={filter.name}
                        value={this.state[filter.id]}
                        onChange={this.handleFilterValueChange}
                    />
                ))}
                <PrimaryButton
                    onClick={this.submitFilters}
                >
                    Filter
                </PrimaryButton>
                <SecondaryButton
                    onClick={this.clearFilters}
                >
                    Clear
                </SecondaryButton>
            </div>
        )
    }
}

FilterList.defaultProps = {
    filters: [],
}

export default FilterList;
