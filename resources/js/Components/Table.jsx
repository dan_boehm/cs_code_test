import {Link, useForm} from "@inertiajs/react";
import React from "react";
import _ from "lodash";
import PrimaryLink from "@/Components/PrimaryLink";
import PrimaryButton from "@/Components/PrimaryButton";

export default function Table({ columns = [], models = [], editRouter, deleteRouter }) {
    const { delete: destroy } = useForm({});
    function getDeleterHandler(model) {
        const route = deleteRouter(model);
        return e => {
            e.preventDefault();
            destroy(route)
        }
    }

    return (
        <div className="overflow-x-auto bg-white rounded shadow">
            <table className="w-full whitespace-nowrap">
                <thead className="text-white bg-gray-600">
                <tr className="font-bold text-left">
                { columns.map(([name, ]) => (
                    <th key={name} className="px-6 pt-5 pb-4">{name}</th>
                )) }
                </tr>
                </thead>
                <tbody>
                {models.map((model) => {
                    return (
                        <tr key={model.id} className="">
                            {columns.map(([, attr]) => (
                                <td className="border-t">
                                    {_.get(model, attr)}
                                </td>
                            ))}
                            {editRouter && (
                                <td key={`${model.id}-edit`} className="border-t">
                                    <PrimaryLink
                                        tabIndex="1"
                                        // className="px-4 py-2 text-sm text-white bg-blue-500 rounded"
                                        href={editRouter(model)}
                                    >
                                        Edit
                                    </PrimaryLink>
                                </td>
                            )}
                            {deleteRouter && (
                                <td  key={`${model.id}-delete`} className="border-t">
                                    <PrimaryButton
                                        tabIndex="1"
                                        className="text-sm text-white bg-red-500 rounded"
                                        onClick={getDeleterHandler(model)}
                                    >
                                        Delete
                                    </PrimaryButton>
                                </td>
                            )}
                        </tr>
                    )
                })}
                {models.length === 0 && (
                    <tr>
                        <td
                            className="px-6 py-4 border-t"
                            colSpan="4"
                        >
                            Nothing found.
                        </td>
                    </tr>
                )}
                </tbody>
            </table>
        </div>
    )
}
