import _ from 'lodash';
import { useForm } from '@inertiajs/react'
import InputLabel from "@/Components/InputLabel";
import InputError from "@/Components/InputError";
import TextInput from "@/Components/TextInput";
import PrimaryButton from "@/Components/PrimaryButton";

function ModelEditor(props) {
    const formValues = props.fields.reduce((values, field) => {
        if (_.has(props.model, field.attr)) {
            values[field.attr] = _.get(props.model, field.attr);

            return values;
        }
    }, {});

    const { data, setData, post, put, processing, errors } = useForm(formValues);

    function handleSubmit(e) {
        e.preventDefault();
        if (props.model.id) {
            put(props.submitRoute)
        } else {
            post(props.submitRoute);
        }
    }

    return (
        <div className="py-12">
            <div className="container mx-auto">
                <form onSubmit={handleSubmit}>
                    { props.fields.map(field => (
                        <div>
                            <InputLabel forInput={field.attr} value={field.label} />
                            <TextInput
                                id={field.attr}
                                type={field.type || "text"}
                                name={field.attr}
                                value={_.get(data, field.attr)}
                                handleChange={e => setData(field.attr, e.target.value)}
                            />
                            <InputError message={errors[field.attr]} />
                        </div>
                    ))}
                    <PrimaryButton processing={processing}>
                        Submit
                    </PrimaryButton>
                </form>
            </div>
        </div>
    );
}

ModelEditor.defaultProps = {
    model: {},
    fields: []
}

export default ModelEditor;
