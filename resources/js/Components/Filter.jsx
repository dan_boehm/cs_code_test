import React from "react";
import TextInput from "@/Components/TextInput";
import InputLabel from "@/Components/InputLabel";

export default function Filter(props) {
    return (
        <div>
            <InputLabel
                forInput={props.id}
                value={props.name}
            />
            <TextInput
                id={props.id}
                key={props.id}
                name={props.id}
                type="text"
                value={props.value}
                onChange={props.onChange}
            />
        </div>
    )
}
