# Getting Started

As a pre-req, setup sail to alias according to the laravel sail docs.  Otherwise, replace with `vendor/bin/sail`

```bash
cp .env.example .env
docker-compose run composer composer install
sail up -d
sail artisan key:generate
sail artisan migrate
sail artisan db:seed
sail npm install
sail npm run dev
```

Then navigate to http://localhost/login
